// SPDX-FileCopyrightText: 2021 Devin Lin <devin@kde.org>
//
// SPDX-License-Identifier: GPL-2.0-or-later

import QtQuick 2.11
import QtQuick.Controls 2.4
import org.kde.kirigami 2.11 as Kirigami

import org.kde.qmlkonsole 1.0

Kirigami.AboutPage {
    id: aboutPage
    aboutData: About
}

